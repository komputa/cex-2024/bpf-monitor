# bpf-monitor

## Running in a Kubernetes cluster

1. Set your kubectl context correctly. e.g. `kubectl config use docker-desktop` when using K8s via Docker Desktop
2. Apply the deamonset: `kubectl apply -f deamonset.yaml` (Or `kubectl apply -f deamonset.local.yaml` to deploy your locally build image named "bp-monitor:latest")

### Using a locally build image

1. Build the image: `docker build -t bpf-monitor:latest .`
2. Apply the deamonset: `kubectl apply -f deamonset.local.yaml`

## Running on the local machine
### Prerequisites
Unix-like OS to either native Linux, WSL2 or MacOS.

> For MacOS take a look at [this](https://aya-rs.dev/book/aya/crosscompile/) to be able to compile.

1. Install `rustup`: https://rustup.rs/
2. Install stable toolchain `rustup install stable`
3. Install nightly toolchain and rust-src `rustup toolchain install nightly --component rust-src`
4. Install bpf-linker: `cargo install bpf-linker`

### Build eBPF

```bash
cargo xtask build-ebpf
```

To perform a release build you can use the `--release` flag.
You may also change the target architecture with the `--target` flag.

### Build Userspace

```bash
cargo build
```

### Build eBPF and Userspace in one go

```bash
cargo xtask build
```

### Run
Either using xtask:
```bash
RUST_LOG=info cargo xtask run
```

Or directly after you build the release binary:
````bash
RUST_LOG=info sudo -E target/release/bpf-monitor
````

To change the interface to monitor you can pass the `--iface` flag:
````bash
RUST_LOG=info sudo -E target/release/bpf-monitor --iface lo
````
or when using xtask:
```bash
RUST_LOG=info cargo xtask run -- --iface lo
```

The default is "eth0", but any network interface of your host should work, e.g. "lo" for loopback.

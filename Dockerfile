FROM rust:bookworm as builder

# Install rustup nightly toolchain which is needed for Aya
RUN rustup toolchain install nightly --component rust-src

# Install BPF linker
RUN cargo install bpf-linker

# Copy the source
COPY .cargo .cargo
COPY bpf-monitor bpf-monitor
COPY bpf-monitor-common bpf-monitor-common
COPY bpf-monitor-ebpf bpf-monitor-ebpf
COPY xtask xtask
COPY Cargo.toml Cargo.toml

# Build the project in release mode
RUN cargo xtask build --release

# Create a new image only with the binary
FROM ubuntu as final

WORKDIR /app

COPY --from=builder --chmod=0755 /target/release/bpf-monitor bpf-monitor

ENV RUST_LOG="info"

CMD ["./bpf-monitor"]
